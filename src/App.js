import React, { useEffect, useState, useMemo } from "react";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

import Home from "./containers/Home"
import Trello from "./containers/Trello"
import Boards from "./containers/Boards"
import NotFound from "./containers/NotFound"

import {AppBar}from "@material-ui/core"
import Style from "./index.module.scss"

function App() {

  useEffect(()=> {
// document.body.contentEditable ="true";
document.title = "Trello Task";
document.body.style.backgroundColor ="darkTurquoise"
document.body.style.margin ="0px"
  },[])


  return (

    <Router>


    <Route
   render={({location}) => {

    return ( 

      <div>
    <AppBar color={"primary"} className={Style.appBar}>
      <Link className={Style.link} to="/">Home</Link>
      <Link className={Style.link} to="/boards">Boards</Link>
    </AppBar>
    
          <Switch>
            <Route path="/" exact component={Home}/>
          <Route path="/boards"  exact component={Boards} />    
          <Route path="/boards/:id" component={Trello} />    
            <Route path="/"  component={NotFound}/>
          </Switch>
      </div>
            )
  }}/>


    </Router>

  );
}



export default App;