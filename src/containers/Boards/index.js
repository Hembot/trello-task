import React, {useEffect, useRef, useState} from 'react'
import Style from "./index.module.scss"
import {Link} from "react-router-dom"
import Card from "@material-ui/core/Card"


function Boards() {
    const [digit, setDigit] = useState("");
    const [bool, setBool] = useState(false)
    const [mapper, setMapper] = useState(["basic first", "basic second"])

const myRef = useRef(null);

    useEffect(()=>{
        setDigit(JSON.stringify(myRef.current.innerHTML));        
    },[bool])

let x;
const saveBoard = () =>{
    x = [...mapper, digit];
localStorage.setItem("saaave", JSON.stringify(x));
}
const load = () =>{
    setMapper(JSON.parse(localStorage.getItem("saaave")))
}

    return (
        <div className={Style.body}>
            <div className={Style.title}>       
<Card > 
<div 
 className={Style.center}
onBlur={()=>setBool(!bool)}
contentEditable={true} 
ref={myRef}
>
write boardName ***here***
  </div>
  </Card>
  <br/>
  <br/>

          Current Board (link) 
          <br/>
          <button onClick={saveBoard}>save name</button>
          <button onClick={load}>add saved names to list</button> :
            <Link className={Style.center} 
            onClick={saveBoard}
            to={`/boards/:${digit}`}>
                 {digit}</Link>
            <br/>

          <br/>
          List of boards:
            <div  className={Style.center}>
                {mapper.map((input, index)=>(
                    <Card>
                    <div  className={Style.center} key={index}> 
board: {index} named: {input}
<Link to={`/boards/:${input}`}> {input}</Link>

          <br/>
                    </div>
                    </Card>
                ))}
            </div>
        </div></div>
    )
}

export default Boards
