import React, {useEffect,useState, useReducer} from 'react'
import Style from "./index.module.scss";
//install node-sass
import {Paper, Card, Button, TextField} from "@material-ui/core"
//installl @material-ui


let list = [[{
    comment:""
},], ]
  
    
function reducer(listState, action) {
    switch (action.type) {
        case "removeBox":
             listState[action.index].splice(action.NextIndex, 1);
             return listState.slice();
        case 'removeList':
                listState.splice(action.index, 1);
return listState.slice()
case 'addAnotherBox':
    let stateCopy = [...listState]; 
    stateCopy[action.index] = listState[action.index] ? [...listState[action.index], {
        comment:""
    }] : [listState]; 
    return stateCopy; 
    case "updateBox":
        let stateCopyr = [...listState];
        stateCopyr[action.index][action.NextIndex].comment = action.text
        return stateCopyr        
    case 'addAntherList':
                return [...listState, [{
                    comment:""
                },]];
case 'pasteBox':
    const copy = [...listState];
    action.store ? copy[action.index].splice(action.NextIndex, 0, action.store): alert("nothing cut")
  return copy;
        case "load":
            let Load;
            if (JSON.parse(localStorage.getItem(action.id)) !== null)
            {
          let Load = JSON.parse(localStorage.getItem(action.id))
                return [...Load]

        } else {
            alert("nothing saved")  
            let Load = [...listState];
            return Load;
          }
        default:
        throw new Error();
    }
  }



  function Trello(props) {      
    let [store, setStore] = useState();
    const [listState, listDispatch] = useReducer(reducer, list);
    const id = props.match.params.id
    const [bool, setBool] = useState(false);


//functions
//Add box
const AddBox = (e, index) => {
    listDispatch({type:"addAnotherBox", index})}   
//add list
const AddList = (e, index) => {
    listDispatch({type:"addAntherList", index: index})
}
//remove list
const RemoveList = (e, index) => {
    listDispatch({type:"removeList", index})}   
//onChange
    const onChange = ( e, NextIndex, index) => {
 listDispatch({type:"updateBox", text:e.target.value, field:"comment", NextIndex, index})}

 return (
        <div className={Style.body}
        style={{display:"flex"}}>
           Current board: {id} 
        <div style={{margin:"0 atuo"}}
        className={Style.container}>
            { listState.map((data, index)=> (
<Paper key={index} className={Style.row} elevation={7}>
<Paper elevation={3} className={Style.cardTitle}>
<h3> &nbsp;column</h3>
</Paper>
<br/>
<div>
{data.map((input, NextIndex)=>(
<div contentEditable={bool} key={NextIndex} 
onBlur={()=>{localStorage.setItem(id,JSON.stringify(listState))}}>
        <Card>
           <b> Box</b>
        </Card>
        <TextField value={data[NextIndex].comment} 
        onChange={(e)=>{onChange(e , NextIndex, index)}}
multiline fullWidth variant="outlined"/>
       list {index} and box {NextIndex}
        <Button onClick={(e) => { 
            listDispatch({type:"removeBox", NextIndex, index})}}>
                Delete Box
            </Button>
            <Button onClick={(e) => { setStore(data[NextIndex]);
            listDispatch({type:"removeBox", NextIndex, index})}}>
                cut Box
            </Button>
            <Button onClick={(e) => { 
            listDispatch({type:"pasteBox", NextIndex, index, store})}}>
                paste Box
            </Button>
        </div>
))}
</div>



<Button onClick={(e)=>{AddBox(e, index)}}> Another Box</Button>
<Button onClick={(e)=>{RemoveList(e, index)}}> Rewmove List</Button>
</Paper>
            ))}           
        </div>
        <Button onClick={AddList}> Another List</Button>
        <Button onClick={(e)=>{listDispatch({type:"load",id})}}>
            Load previous save
        </Button>
        <Button onClick={(e)=>{localStorage.setItem(id,JSON.stringify(listState))}}>
            Save
        </Button>
        </div>
    )
}

export default Trello
